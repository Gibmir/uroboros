package uroboros.mojo.resolver;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class ClassResolver {
  private static final Logger log = LoggerFactory.getLogger(ClassResolver.class);
  private static final int COMPILATION_UNIT_TYPE_INDEX = 0;
  private static final int WITHOUT_PACKAGE_SUBSTRING_INDEX = 8;

  /**
   * Resolves classes by represented compilation units.
   *
   * @param units compilation units
   * @return resolved classes
   * @throws ClassNotFoundException if class was not loaded
   */
  public List<Class<?>> resolveClassesBy(Collection<CompilationUnit> units)
      throws ClassNotFoundException {
    List<Class<?>> loadedClasses = new ArrayList<>();
    for (CompilationUnit unit : units) {
      final String className = resolveClassNameFor(unit);
      final Class<?> loadedClass = Class.forName(className);
      loadedClasses.add(loadedClass);
    }
    return loadedClasses;
  }

  private String resolveClassNameFor(CompilationUnit unit) {
    final TypeDeclaration<?> type = unit.getType(COMPILATION_UNIT_TYPE_INDEX);
    if (type != null) {
      final Optional<PackageDeclaration> packageDeclaration = unit.getPackageDeclaration();
      if (packageDeclaration.isPresent()) {
        final String packageStringRepresentation = packageDeclaration.get().toString().trim();
        log.debug(
            "Starting load class by type:[{}] that declared in package:[{}]",
            type.getName(),
            packageStringRepresentation);
        return getClassNameBy(packageStringRepresentation, type);
      } else {
        log.debug("Starting load class for type:[{}]", type);
        return getClassNameFor(type);
      }
    } else {
      log.debug("The type declaration for unit:[{}] is null", unit);
      throw new NullPointerException(
          String.format("The type declaration for unit %s is null", unit));
    }
  }

  private String getClassNameBy(String packageStringRepresentation, TypeDeclaration<?> type) {
    return removePackageKeyword(packageStringRepresentation)
        .trim()
        .replace(';', '.')
        .concat(getClassNameFor(type));
  }

  private String getClassNameFor(TypeDeclaration<?> type) {
    return type.getNameAsString();
  }

  private String removePackageKeyword(String packageStringRepresentation) {
    return packageStringRepresentation.substring(WITHOUT_PACKAGE_SUBSTRING_INDEX);
  }
}
