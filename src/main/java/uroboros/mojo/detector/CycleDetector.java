package uroboros.mojo.detector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uroboros.mojo.detector.dependency.JavaDependenciesTree;

import java.util.Collection;

public class CycleDetector {
  private static Logger log = LoggerFactory.getLogger(CycleDetector.class);

  /**
   * Detects cycle for specified classes.
   *
   * @param classes classes for analyze
   *
   */
  public void detectCyclesIn(Collection<Class<?>> classes) {
    log.info("The classes for scanning: {}", classes);
    for (Class<?> dependencyClass : classes) {
      JavaDependenciesTree.tryBuildBy(dependencyClass);
    }
  }
}
