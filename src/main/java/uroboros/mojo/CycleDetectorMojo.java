package uroboros.mojo;

import com.github.javaparser.ast.CompilationUnit;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.classworlds.realm.ClassRealm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uroboros.mojo.detector.CycleDetector;
import uroboros.mojo.reader.JavaFilesReader;
import uroboros.mojo.resolver.ClassResolver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Mojo(
    name = "detect",
    defaultPhase = LifecyclePhase.VERIFY,
    requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
@Execute(goal = "detect", phase = LifecyclePhase.VERIFY)
public class CycleDetectorMojo extends AbstractMojo {
  private static final Logger log = LoggerFactory.getLogger(CycleDetectorMojo.class);

  @Parameter(property = "dir", defaultValue = "${project.build.sourceDirectory}")
  private String scanDir;

  @Parameter(defaultValue = "${project}", readonly = true)
  private MavenProject mavenProject;

  @Parameter(defaultValue = "${localRepository}", readonly = true)
  private ArtifactRepository localRepository;

  @Override
  public void execute() {
    log.info("Dir for scanning: [{}]", scanDir);
    try {
      appendProjectArtifactsToClassLoader();
      final Path scanPath = Paths.get(scanDir);
      final JavaFilesReader javaFilesReader = new JavaFilesReader();
      final ClassResolver classResolver = new ClassResolver();
      final CycleDetector cycleDetector = new CycleDetector();
      final Collection<CompilationUnit> compilationUnits = javaFilesReader.readFrom(scanPath);
      final List<Class<?>> classes = classResolver.resolveClassesBy(compilationUnits);
      cycleDetector.detectCyclesIn(classes);
    } catch (Exception e) {
      log.error("Exception occurred while analyzing dir {}. Exception:", scanDir, e);
    }
  }

  private void appendProjectArtifactsToClassLoader() {
    final PluginDescriptor pluginDescriptor =
        (PluginDescriptor) getPluginContext().get("pluginDescriptor");
    final ClassRealm classRealm = pluginDescriptor.getClassRealm();
    try {
      final File scanningClasses = new File(mavenProject.getBuild().getOutputDirectory());
      final URL url = scanningClasses.toURI().toURL();
      classRealm.addURL(url);
      final List<URL> artifactPaths = getAllProjectArtifactPaths(mavenProject);
      for (URL artifactPath : artifactPaths) {
        classRealm.addURL(artifactPath);
      }
    } catch (MalformedURLException e) {
      log.error("Error while scan url:", e);
    }
  }

  private List<URL> getAllProjectArtifactPaths(MavenProject mavenProject)
      throws MalformedURLException {
    List<URL> artifactPaths = new ArrayList<>();
    MavenProject parent = mavenProject;
    if (parent.hasParent()) {
      while (parent.hasParent()) {
        addUrls(artifactPaths, parent);
        parent = parent.getParent();
      }
    } else {
      addUrls(artifactPaths, parent);
    }
    return artifactPaths;
  }

  private void addUrls(List<URL> artifactPaths, MavenProject parent) throws MalformedURLException {
    final Set<Artifact> dependencyArtifacts = parent.getArtifacts();
    log.info(
        "Project {} information:\n Dependencies:{}.\n Artifacts:{}",
        parent.getName(),
        parent.getDependencies(),
        dependencyArtifacts);
    for (Artifact dependencyArtifact : dependencyArtifacts) {
      final String artifactPath = getArtifactPath(dependencyArtifact);
      artifactPaths.add(new File(artifactPath).toURI().toURL());
    }
  }

  private String getArtifactPath(Artifact dependencyArtifact) {
    return localRepository.getBasedir()
        + File.separator
        + localRepository.pathOf(dependencyArtifact);
  }
}
