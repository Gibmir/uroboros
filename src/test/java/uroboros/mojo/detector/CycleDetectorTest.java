package uroboros.mojo.detector;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import uroboros.mojo.detector.dependency.CycleException;
import uroboros.mojo.stub.classes.A;
import uroboros.mojo.stub.classes.recursive.Recursive;

import java.util.Collections;

class CycleDetectorTest {

  private static final CycleDetector CYCLE_DETECTOR = new CycleDetector();

  @Test
  void recursiveDoesNotThrow() {
    Assertions.assertDoesNotThrow(
        () -> CYCLE_DETECTOR.detectCyclesIn(Collections.singleton(Recursive.class)));
  }

  @Test
  void verify() {
    final String expectedMessage =
        "Cycle detected in field of type uroboros.mojo.stub.classes.A This field contains in uroboros.mojo.stub.classes.C";
    Assertions.assertThrows(
        CycleException.class,
        () -> CYCLE_DETECTOR.detectCyclesIn(Collections.singleton(A.class)),
        expectedMessage);
  }
}
