package uroboros.mojo.detector.dependency;

public class CycleException extends RuntimeException {
  public CycleException(String message) {
    super(message);
  }
}
