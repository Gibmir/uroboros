package uroboros.mojo.reader;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import uroboros.mojo.reader.exception.InputOutputRuntimeException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaFilesReader {
  private static final String JAVA_FILE_EXTENSION = ".java";
  private final JavaParser parser = new JavaParser();

  /**
   * Read all java files from specified dir.
   *
   * @param dir directory path with java files for analyze
   * @return collection of java files for analyze
   * @throws IOException if can't read specified directory
   */
  public Collection<CompilationUnit> readFrom(Path dir) throws IOException {
    return read(dir);
  }

  private Collection<CompilationUnit> read(Path dir) throws IOException {
    try (Stream<Path> dirPaths = Files.walk(dir)) {
      return dirPaths
          .filter(path -> path.toString().endsWith(JAVA_FILE_EXTENSION))
          .map(this::readJavaFile)
          .map(parser::parse)
          .filter(ParseResult::isSuccessful)
          .map(ParseResult::getResult)
          .filter(Optional::isPresent)
          .map(Optional::get)
          .collect(Collectors.toList());
    }
  }

  private String readJavaFile(Path javaFilePath)  {
    try (Stream<String> lines = Files.lines(javaFilePath)) {
      return lines.collect(Collectors.joining());
    } catch (IOException e) {
      throw new InputOutputRuntimeException(e);
    }
  }
}
