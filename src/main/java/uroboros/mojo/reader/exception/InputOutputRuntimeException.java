package uroboros.mojo.reader.exception;

public class InputOutputRuntimeException extends RuntimeException {
  public InputOutputRuntimeException(Throwable cause) {
    super(cause);
  }
}
