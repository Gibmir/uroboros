package uroboros.mojo.reader;

import com.github.javaparser.ast.CompilationUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import uroboros.mojo.stub.classes.recursive.Recursive;
import uroboros.mojo.util.PathUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;

class JavaFilesReaderTest {
  private static final String WRONG_DIR = "/etc/wrong/dir";
  private static final JavaFilesReader JAVA_FILES_READER = new JavaFilesReader();
  private static final Class<Recursive> TEST_CLASS = Recursive.class;

  @Test
  void testReadFromRecursive() throws IOException {
    final Path recursiveClassPath = PathUtils.getPathOfTest(TEST_CLASS);
    final Collection<CompilationUnit> compilationUnits =
        JAVA_FILES_READER.readFrom(recursiveClassPath);
    Assertions.assertNotNull(compilationUnits);
    Assertions.assertEquals(1, compilationUnits.size());
    final CompilationUnit unit = compilationUnits.iterator().next();
    Assertions.assertNotNull(unit);
    Assertions.assertTrue(unit.toString().contains(TEST_CLASS.getSimpleName()));
  }
}
