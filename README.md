# Project uroboros

This project represents maven plugin for checking
cycle dependencies between classes in your project. 
It loads classes of your project and dependencies classes so be aware
 that this plugin works on already built project
---
## Instructions

To start checking your api you need to add this plugin into your pom.xml  like this:

```xml
 <plugin>
    <groupId>project-uroboros</groupId>
    <artifactId>uroboros</artifactId>
    <version>0.1</version>
 </plugin>
```

after that you can check your api in the verify phase or you can simply use command (if project was built):

```
mvn uroboros:detect
```

from the root of your maven project.