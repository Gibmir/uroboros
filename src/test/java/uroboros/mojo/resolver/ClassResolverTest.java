package uroboros.mojo.resolver;

import com.github.javaparser.ast.CompilationUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import uroboros.mojo.reader.JavaFilesReader;
import uroboros.mojo.stub.classes.recursive.Recursive;
import uroboros.mojo.util.PathUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

class ClassResolverTest {
  private static final Class<Recursive> TEST_CLASS = Recursive.class;
  private static final JavaFilesReader JAVA_FILES_READER = new JavaFilesReader();
  private static final ClassResolver CLASS_RESOLVER = new ClassResolver();

  @Test
  void testResolveClassesByTestCompilationUnits() throws ClassNotFoundException, IOException {

    final Collection<CompilationUnit> compilationUnits = JAVA_FILES_READER.readFrom(PathUtils.getPathOfTest(TEST_CLASS));
    final List<Class<?>> classes = CLASS_RESOLVER.resolveClassesBy(compilationUnits);
    Assertions.assertNotNull(classes);
    Assertions.assertEquals(1,classes.size());
    final Class<?> aClass = classes.get(0);
    Assertions.assertEquals(TEST_CLASS, aClass);
  }
}
