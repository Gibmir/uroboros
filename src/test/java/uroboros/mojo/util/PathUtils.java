package uroboros.mojo.util;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathUtils {
  private static final String USER_DIR = System.getProperty("user.dir");
  private static final String MAVEN_PROJECT_DIR =
      USER_DIR
          + File.separator
          + "src"
          + File.separator
          + "test"
          + File.separator
          + "java"
          + File.separator;
  private static final String EMPTY_STRING = "";
  private static final String DOT = "\\.";
  private static final String JAVA = ".java";

  private PathUtils() {}

  public static Path getPathOfTest(Class<?> testClass) {
    final String[] split = testClass.getName().split(DOT);
    final String classPath = String.join(File.separator, split);
    return Paths.get(MAVEN_PROJECT_DIR + classPath + JAVA);
  }
}
