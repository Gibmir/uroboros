package uroboros.mojo.detector.dependency;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public class JavaDependenciesTree {

  /**
   * Trying to build dependency tree.
   *
   * @param dependencyClass {@link Class} that describes dependency
   * @throws CycleException if there are cycles in {@link Class the dependency}
   */
  public static void tryBuildBy(Class<?> dependencyClass) {
    new JavaDependenciesTree(dependencyClass);
  }

  private JavaDependenciesTree(Class<?> dependency) {
    DependencyNode root = new DependencyNode(dependency);
    tryBuild(root);
  }

  private void tryBuild(DependencyNode node) {
    Queue<DependencyNode> nodes = new ArrayDeque<>();
    nodes.add(node);
    while (!nodes.isEmpty()) {
      final DependencyNode dependency = nodes.poll();
      dependency.children = getChildren(dependency);
      final List<DependencyNode> nonRecursive =
          Arrays.stream(dependency.children)
              .filter(child -> !dependency.nodeClass.equals(child.nodeClass))
              .filter(child -> !child.nodeClass.isMemberClass())
              .collect(Collectors.toList());
      for (DependencyNode child : nonRecursive) {
        check(dependency, child);
      }
      nodes.addAll(nonRecursive);
    }
  }

  private DependencyNode[] getChildren(DependencyNode parent) {
    final List<Field> nonTransitiveFields = getFilteredFieldsOf(parent);
    DependencyNode[] children = new DependencyNode[nonTransitiveFields.size()];
    for (int i = 0; i < children.length; i++) {
      children[i] = new DependencyNode(nonTransitiveFields.get(i).getType());
      children[i].parent = parent;
    }
    return children;
  }

  private List<Field> getFilteredFieldsOf(DependencyNode parent) {
    return Arrays.stream(parent.nodeClass.getDeclaredFields())
        .filter(this::isNotTransient)
        .collect(Collectors.toList());
  }

  private boolean isNotTransient(Field field) {
    return !Modifier.isTransient(field.getModifiers());
  }

  private void check(DependencyNode dependency, DependencyNode child) {
    try {
      child.check();
    } catch (CycleException e) {
      throw new CycleException(
          String.format(
              "%s This field contains in %s", e.getMessage(), dependency.nodeClass.getName()));
    }
  }

  private static class DependencyNode {
    private final Class<?> nodeClass;
    private DependencyNode parent;
    private DependencyNode[] children;

    private DependencyNode(Class<?> nodeClass) {
      this.nodeClass = nodeClass;
    }

    public void check() {
      DependencyNode node = this.parent;
      while (node != null) {
        for (DependencyNode child : this.parent.children) {
          if ((node.nodeClass.equals(child.nodeClass))
              && (!node.nodeClass.equals(parent.nodeClass))) {
            throw new CycleException(
                String.format("Cycle detected in field of type %s", child.nodeClass.getName()));
          }
        }
        node = node.parent;
      }
    }
  }
}
